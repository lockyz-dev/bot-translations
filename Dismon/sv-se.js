require('dotenv-flow').config();

module.exports = {
    /*
        Svensk översättning för Dismon Discord Botten
    */
    
    //Global
    searchError: "Denna sökning har resulterat i ett fel",

    //Info Command
    //Currently unavailable

    //ItemDex Command
    itemUnprovided: "Ett objekt måste anges",
    itemEffect: "Effekt",
    itemMartCost: "Kostnad i Mart",
    itemMartUnpurchaseable: "Inte Köpbar",
    itemCategory: "Objekt Kategori",

    //Metrics Command
    cmdMetricsDisabled: "Detta kommando har inte mätvärden aktiverade.",
    metricsUses: "Mängd andvändningar",

    //PokeDex Command
    pokeUnprovided: "En pokemon måste anges",
    pokeTypes: "Typer",
    pokeHeight: "Högd",
    pokeWeight: "Vikt",
    pokeExerienceBase: "Base Experience",
    pokeAbility: "Förmåga",
    pokeHiddenAbility: "Dold Förmåga",

    //Settings Command
    //Currently unavailable

    //Typedex Command
    typeCMDUnprovided: "En typ måste anges",
    typeCMDStrongAgainst: "Stark emot",
    typeCMDWeakAgainst: "Svag emot",

    //Types
    typeBug: "Insekter (bug)",
    typeDark: "Mörker (Dark)",
    typeDragon: "Drakar (Dragon)",
    typeElectric: "Elektricitet (Electric)",
    typeFairy: "Fe (Fairy)",
    typeFighting: "Kamp (Fighting)",
    typeFire: "Eld (Fire)",
    typeFlying: "Flygande (Flying)",
    typeGhost: "Spöke (Ghost)",
    typeGrass: "Gräs (Grass)",
    typeGround: "Mark (Ground)",
    typeIce: "Is (Ice)",
    typeNormal: "Normal",
    typePoison: "Gift (Poison)",
    typePsychic: "Psykisk (Psychic)",
    typeRock: "Sten (Rock)",
    typeSteel: "Stål (Steel)",
    typeWater: "Vatten (Water)"


    //NOTE, MANY strings are currently excluded. They will be added as we continue to work on translation integration.
}
