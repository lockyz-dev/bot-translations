require('dotenv-flow').config();

module.exports = {
    /*
        Template for Translation files for the Dismon Discord Bot
    */
    
    //Global
    searchError: "",

    //Info
    //Currently unavailable

    //ItemDex Command
    itemUnprovided: "",
    itemEffect: "",
    itemMartCost: "",
    itemMartUnpurchaseable: "",
    itemCategory: "",

    //Metrics Command
    cmdMetricsDisabled: "",
    metricsUses: "",

    //PokeDex Command
    pokeUnprovided: "",
    pokeTypes: "",
    pokeHeight: "",
    pokeWeight: "",
    pokeExerienceBase: "",
    pokeAbility: "",
    pokeHiddenAbility: "",

    //Settings Command
    //Currently unavailable

    //Typedex Command
    typeCMDUnprovided: "",
    typeCMDStrongAgainst: "",
    typeCMDWeakAgainst: "",

    //Types
    typeBug: "",
    typeDark: "",
    typeDragon: "",
    typeElectric: "",
    typeFairy: "",
    typeFighting: "",
    typeFire: "",
    typeFlying: "",
    typeGhost: "",
    typeGrass: "",
    typeGround: "",
    typeIce: "",
    typeNormal: "",
    typePoison: "",
    typePsychic: "",
    typeRock: "",
    typeSteel: "",
    typeWater: ""


    //NOTE, MANY strings are currently excluded. They will be added as we continue to work on translation integration.
}
