# At this current time Both Dismon and JoiBoi ONLY support en-au, we're working on fixing this asap

# Bot Translations

This repository hosts the translation files for all* the bots owned and managed by Lockyz Dev

Many strings are currently not able to be translated. We're working on it.

### Bots Added
- Dismon
- JoiBoi

### Languages Currently Supported
- en-au (English) *Dismon, JoiBoi*

### Languages being worked on
- es-ES (Spanish) *Dismon, JoiBoi*
- sv-se (Swedish) *Dismon, JoiBoi*

## Current Contributers
- Robin "Lockyz" Painter (en-au)

## FAQ
### I helped translate, where's my name?
There are multiple ways to be listed in this repository.
1 - Create the base translation for any language (Basically doing the first translation into a new language).
2 - MANY substantial contributions.

### How to contribute?
Currently you can create a Merge Request for new languages or strings.

### How to request a new language?
You can request a new language in our discord server https://discord.gg/eRPsZns

### Where's en-us or en-gb?
We've made the decision to not support different dialects of the English Language.
At this current time we're only going to support the Australian English version.

**Private Bots are only included if permission is given*
